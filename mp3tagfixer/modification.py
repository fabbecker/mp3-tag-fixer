import logging

from mutagen.easyid3 import EasyID3

import mp3tagfixer.analysis


def get_mp3_attributes(path):
    """Returns the attributes of an mp3 file as dict."""
    af = EasyID3(str(path))
    tags_raw = {
        x.lower(): " & ".join(af[x]) for x in af.keys()
    }
    tags = {}
    for x in tags_raw:
        try:
            tags[x] = float(tags_raw[x])
        except ValueError:
            tags[x] = tags_raw[x]
    if "bpm" not in tags:
        tags["bpm"] = mp3tagfixer.analysis.get_bpm(path)
    if "artist" not in tags:
        tags["artist"] = mp3tagfixer.analysis.get_artist(path)
    if "title" not in tags:
        tags["title"] = mp3tagfixer.analysis.get_title(path)
    return tags


def set_mp3_attributes(path: str, **kwargs):
    """Sets the attributes of an mp3 file"""
    af = EasyID3(f"{path}")
    for v in kwargs:
        if v in kwargs and kwargs[v]:
            if type(kwargs[v]) == str:
                af[v] = kwargs[v].split(" & ")
            elif type(kwargs[v]) == list:
                af[v] = kwargs[v]
            else:
                try:
                    af[v] = [str(kwargs[v])]
                except Exception:
                    raise ValueError(f"Value cannot be {type(kwargs[v])} ({kwargs[v]})")
            logging.info(f"Set \"{path}\"'s {v} to {kwargs[v]}")
    af.save()

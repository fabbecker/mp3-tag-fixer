import os.path

from aubio import tempo, source
from numpy import median, diff
import heapq


separators = [
    "x", "X",
    "&", "and",
    "vs", "vs.",
    "ft", "ft.", "feat", "feat.", "featuring",
    "with", "w/",
    "presents",
    ", ",
]


def get_file_name(path: str):
    return str(path).split("/")[-1].strip()


def get_description(path):
    return ".".join(get_file_name(path).split(".")[0:-1]).strip()


def get_artist(path: str, first: bool = False):
    """
    path: Path to the file
    first: return only the first artist instead of all artists
    """
    artist = get_description(path).split(" - ")[0]
    if first:
        for x in separators:
            artist = artist.split(f" {x} ")[0].strip()
    return artist


def get_title(path: str):
    return " - ".join(get_description(path).split(" - ")[1:]).strip()


def get_format(path: str):
    return path.split(".")[-1].strip()


def get_bpm(path: str, params=None):
    """ Calculate the beats per minute (bpm) of a given file.
        https://github.com/aubio/aubio/blob/master/python/demos/demo_bpm_extract.py
        path: path to the file
        param: dictionary of parameters
    """

    if params is None:
        params = {}
    # default:
    samplerate, win_s, hop_s = 44100, 1024, 512
    if 'mode' in params:
        if params.mode in ['super-fast']:
            # super fast
            samplerate, win_s, hop_s = 4000, 128, 64
        elif params.mode in ['fast']:
            # fast
            samplerate, win_s, hop_s = 8000, 512, 128
        elif params.mode in ['default']:
            pass
        else:
            raise ValueError("unknown mode {:s}".format(params.mode))
    # manual settings
    if 'samplerate' in params:
        samplerate = params.samplerate
    if 'win_s' in params:
        win_s = params.win_s
    if 'hop_s' in params:
        hop_s = params.hop_s

    s = source(os.path.expanduser(str(path)), samplerate, hop_s)
    samplerate = s.samplerate
    o = tempo("specdiff", win_s, hop_s, samplerate)
    # List of beats, in samples
    beats = []
    confidence = []
    # Total number of frames read
    total_frames = 0

    while True:
        samples, read = s()
        is_beat = o(samples)
        if is_beat:
            this_beat = o.get_last_s()
            beats.append(this_beat)
            confidence.append(o.get_confidence())
        total_frames += read
        if read < hop_s:
            break

    # Do not use average bpm
    def get_average_bpm(beats):
        # if enough beats are found, convert to periods then to bpm
        if len(beats) > 1:
            if len(beats) < 4:
                print("few beats found in {:s}".format(path))
            bpms = 60./diff(beats)
            return median(bpms)
        else:
            print("not enough beats found in {:s}".format(path))
            return 0

    # Use most dominant bpm instead (Mixxx seems to be using this one too)
    def get_main_bpm(beats):
        intervals = []
        for i in range(len(beats)):
            intervals.append(max(0, beats[i] - beats[i-1]))
        speeds = [round(60 / x) if x != 0 else 0 for x in intervals]

        sorted_histo = []
        for i in range(len(beats)):
            heapq.heappush(sorted_histo, (confidence[i], speeds[i]))
        res = max(sorted_histo)[-1]
        return res

    return get_main_bpm(beats)

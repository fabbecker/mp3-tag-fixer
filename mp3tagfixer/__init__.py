import pathlib
from mp3tagfixer.modification import get_mp3_attributes, set_mp3_attributes
from mp3tagfixer import analysis


def set_multiple_file_data(path: [pathlib.PosixPath, str], **force_attributes):
    if type(path) == str:
        path = pathlib.PosixPath(path)
    path = path.expanduser()
    if path.exists():
        if path.is_dir():
            for new_path in path.iterdir():
                set_multiple_file_data(path=new_path, **force_attributes)
        elif path.is_file() and str(path).endswith(".mp3"):
            try:
                set_automatic(path, **force_attributes)
            except RuntimeError as e:
                print(f"WARN: failed to process {path}")
                print(e)


def set_automatic(path: pathlib.PosixPath, **force_attributes):
    if force_attributes is None:
        force_attributes = {}
    data = {
        **{
            "bpm": str(analysis.get_bpm(str(path))),
        },
        **{
            **get_mp3_attributes(str(path)),
            **force_attributes,
        },
    }
    print(f"Data: {data}")
    set_mp3_attributes(
        str(path),
        **data
    )

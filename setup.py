from setuptools import setup, find_packages

setup(
    name="mp3tagfixer",
    version="0.1",
    packages=find_packages(),
    license="GNU GPLv3",
)

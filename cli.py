import argparse
import glob
import pathlib
import logging

import mp3tagfixer


parser = argparse.ArgumentParser(description="Add missing attributes to your mp3 files.")
parser.add_argument("-a", "--artist", help="The alias of the artist who performed this recording.")
parser.add_argument("-t", "--title", help="The title of this song.")
parser.add_argument("-c", "--album", help="The album in which this song has been released.")
parser.add_argument("-s", "--bpm", help="Force a specific speed in beats per minute.")
parser.add_argument("-r", "--read", help="Read the attributes from the file instead of writing anything")
parser.add_argument("path", help="The file or folder to manipulate.", type=pathlib.Path, nargs="+")


logging.basicConfig(level=logging.DEBUG)


def get_forced_attributes(args) -> dict:
    return {x: args.__dict__[x] for x in args.__dict__ if args.__dict__[x] is not None and x != "path"}


def cli(args):
    forced_attributes = get_forced_attributes(args)

    empty_check = {
        "artist": mp3tagfixer.analysis.get_artist,
        "title": mp3tagfixer.analysis.get_title,
        "bpm": mp3tagfixer.analysis.get_bpm,
    }

    for p in args.path:
        for path in [pathlib.PosixPath(e) for e in glob.glob(str(p))]:  # allow * patterns
            if path.exists() and str(path).endswith(".mp3"):
                logging.debug(f"Working on {str(path)}")
                forced_attributes_local = forced_attributes.copy()
                for arg in forced_attributes_local:
                    if forced_attributes_local[arg] == "_":
                        try:
                            forced_attributes_local[arg] = empty_check[arg](str(path))
                        except IndexError:
                            del forced_attributes_local[arg]

                mp3tagfixer.set_mp3_attributes(path=str(path), **forced_attributes_local)
                logging.debug(f"Set values: {forced_attributes_local}")


cli(parser.parse_args())
